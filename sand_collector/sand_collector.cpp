#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <conio.h>
#include <Windows.h>
#include <vector>

void gravity_pull(sf::Sprite &pSprite, sf::RectangleShape &pHitBox, sf::RectangleShape Brick[500], sf::RectangleShape Steel[200], sf::RectangleShape Ladder[20], int brick_counter, int steel_counter, int ladder_counter, sf::Clock &AccelClock, float acceleration)
{
	bool pull = true;

	pHitBox.setPosition(pHitBox.getPosition().x, pHitBox.getPosition().y + 1);
	for (int i = 0; i < steel_counter; i++)
	{
		if (pHitBox.getGlobalBounds().intersects(Steel[i].getGlobalBounds()))
		{
			pSprite.setPosition(pSprite.getPosition().x, Steel[i].getPosition().y - 32);
			pull = false;
			break;
		}
	}
	for(int i = 0; i < brick_counter; i++)
	{
		if(pHitBox.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
		{
			pSprite.setPosition(pSprite.getPosition().x, Brick[i].getPosition().y - 32);
			pull = false;
			break;
		}
	}
	
	for(int i = 0; i < ladder_counter; i++)
	{
		if(pHitBox.getGlobalBounds().intersects(Ladder[i].getGlobalBounds()))
		{
			pull = false;
			break;
		}
	}
	pHitBox.setPosition(pHitBox.getPosition().x, pHitBox.getPosition().y - 1);
	if(pull)
	{
		pSprite.move(0, acceleration * AccelClock.getElapsedTime().asSeconds());
	}
	else
	{
		AccelClock.restart();
	}
}

void main()
{
	srand(time(0));
	bool stage_load = true, pause = false, stage_next = true, gameover = false;
	int timer = 300, timer_second, timer_minute, completed_staged[100];
	int map[100][100], stage_number = 0, ladder_counter = 0, brick_counter = 0, steel_counter = 0, BrickCd[500], BrickPositionY[500], BrickPositionX[500], sand_amount, sand_collected, completed_stage = 0, difficulty = 1;
	float acceleration = 20;
	sf::RenderWindow Window(sf::VideoMode(800,600),"Sand Collector",sf::Style::Fullscreen);
	sf::Sprite pSprite;
	sf::Texture pTexture;

	sf::Font Font;
	sf::Text timer_text;

	std::string timer_sentence;
	std::ostringstream convert_second, convert_minute;

	timer_minute = timer / 60;
	timer_second = timer - timer_minute * 60;

	convert_minute << timer_minute;
	convert_second << timer_second;

	if (timer_second < 10)
	{
		timer_sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
	}
	else
	{
		timer_sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
	}

	timer_text.setFont(Font);
	timer_text.setCharacterSize(32);
	timer_text.setColor(sf::Color(255, 255, 255, 255));
	timer_text.setPosition(608, 26);
	timer_text.setString(timer_sentence);

	pTexture.loadFromFile("p1.png");
	pSprite.setTexture(pTexture);

	enum pDirection {Down, Left, Right, Up};
	float frameCounter,switchFrame,frameSpeed;

	frameCounter = 0;
	switchFrame = 200;
	frameSpeed = 800;

	sf::Vector2i Source;
	Source.x = 1;
	Source.y = Down;
	
	std::ostringstream ConvertSandNum;
	sf::Text SandNumber;
	sf::RectangleShape Brick[500], Steel[200], Ladder[200], pHitBox, Door, Sand[200], SandDisplay, DoorMedium, DoorHard, pBoundaryLeft, pBoundaryRight, pBoundaryUp, pBoundaryDown;
	sf::Texture BrickTexture, LadderTexture, DoorClose, DoorOpen, SandTexture, SteelTexture;

	SteelTexture.loadFromFile("steel.jpg");
	for (int i = 0; i < 200; i++)
	{
		Steel[i].setSize(sf::Vector2f(32, 32));
		Steel[i].setTexture(&SteelTexture);
	}

	SandNumber.setFont(Font);
	SandNumber.setCharacterSize(32);
	SandNumber.setColor(sf::Color(255, 255, 255, 255));
	SandNumber.setPosition(662, 186);

	pHitBox.setSize(sf::Vector2f(16,16));
	pBoundaryLeft.setSize(sf::Vector2f(16, 16));
	pBoundaryRight.setSize(sf::Vector2f(16, 16));
	pBoundaryUp.setSize(sf::Vector2f(24, 18));
	pBoundaryDown.setSize(sf::Vector2f(24, 18));

	BrickTexture.loadFromFile("brick32.png");
	for(int i = 0; i < 500; i++)
	{
		BrickCd[i] = 0;
		Brick[i].setSize(sf::Vector2f(32, 32));
		Brick[i].setTexture(&BrickTexture);
	}

	LadderTexture.loadFromFile("ladder.png");
	for(int i = 0; i < 200; i++)
	{
		Ladder[i].setSize(sf::Vector2f(32,32));
		Ladder[i].setTexture(&LadderTexture);
	}

	SandTexture.loadFromFile("sand32.png");
	for (int i = 0; i < 200; i++)
	{
		Sand[i].setSize(sf::Vector2f(32, 32));
		Sand[i].setTexture(&SandTexture);
	}

	SandDisplay.setSize(sf::Vector2f(32, 32));
	SandDisplay.setTexture(&SandTexture);
	SandDisplay.setPosition(620, 186);

	DoorClose.loadFromFile("doorclose.png");
	DoorOpen.loadFromFile("dooropen.png");
	Door.setSize(sf::Vector2f(32, 32));
	Door.setTexture(&DoorClose);

	DoorMedium.setSize(sf::Vector2f(32, 32));
	DoorMedium.setTexture(&DoorClose);
	DoorMedium.setFillColor(sf::Color(0, 191, 255, 255));

	DoorHard.setSize(sf::Vector2f(32, 32));
	DoorHard.setTexture(&DoorClose);
	DoorHard.setFillColor(sf::Color(255, 160, 122, 255));

	sf::Text PauseText;

	PauseText.setFont(Font);
	PauseText.setCharacterSize(32);
	PauseText.setColor(sf::Color(255, 255, 255, 255));
	PauseText.setString("Paused");
	PauseText.setPosition(620, 250);

	sf::Clock Clock, AccelClock, RespawnClock;

	sf::Text StageText, Minute, Second;
	std::ostringstream ConvertStage;

	Font.loadFromFile("font.otf");
	StageText.setFont(Font);
	StageText.setCharacterSize(32);
	StageText.setColor(sf::Color(255, 255, 255, 255));
	StageText.setPosition(620, 122);

	sf::Vector2i mapSize;

	mapSize.x = 17;
	mapSize.y = 19;

	Window.setFramerateLimit(60);
	Window.setKeyRepeatEnabled(false);
	FreeConsole();
	while (Window.isOpen())
	{
		sf::Event Event;

		if (stage_load == true)
		{
			bool searching = true;
			ConvertStage.str("");
			ConvertStage.clear();
			ConvertStage << int(completed_stage);

			StageText.setString("Stage : " + ConvertStage.str());

			Door.setTexture(&DoorClose);
			Door.setPosition(-1000, -1000);
			DoorMedium.setPosition(-1000, -1000);
			DoorHard.setPosition(-1000, -1000);

			for (int i = 0; i < 500; i++)
			{
				Brick[i].setPosition(-1000, -1000);
				if (i < 200)
				{
					Sand[i].setPosition(-1000, -1000);
					Ladder[i].setPosition(-1000, -1000);
					Steel[i].setPosition(-1000, -1000);
				}
			}

			std::ifstream openmap("mapcomp.txt");

			if (completed_stage != 0)
			{
				if (stage_next)
				{
					if (completed_stage <= 3)
					{
						while (searching)
						{
							stage_number = rand() % 5 + 1;
							for (int i = 0; i < completed_stage; i++)
							{
								if (stage_number == completed_staged[i])
								{
									searching = true;
									break;
								}
								else
								{
									searching = false;
								}
							}
						}
					}
					else
					{
						stage_number = rand() % 3 + 11;
					}
				}
			}

			if (difficulty == 1)
			{
				if (completed_stage == 4)
				{
					stage_number = 99;
				}
			}
			else if (difficulty == 2)
			{
				if (completed_stage == 5)
				{
					stage_number = 99;
				}
			}
			else
			{
				if (completed_stage == 6)
				{
					stage_number = 99;
				}
			}

			brick_counter = 0;
			ladder_counter = 0;
			sand_amount = 0;
			sand_collected = 0;
			steel_counter = 0;

			if (openmap.is_open())
			{
				int stage_num_scan;
				std::string trash;
				openmap >> stage_num_scan;
				while (stage_num_scan != stage_number)
				{
					for (int i = 0; i < mapSize.y + 1; i++)
					{
						std::getline(openmap, trash);
					}
					openmap >> stage_num_scan;
				}

				for (int i = 0; i < mapSize.y; i++)
				{
					for (int j = 0; j < mapSize.x; j++)
					{
						openmap >> map[i][j];
						if (map[i][j] == 5)
						{
							pSprite.setPosition(j * 32, i * 32);
							pHitBox.setPosition(j * 32, i * 32);
						}
						else if (map[i][j] == 2)
						{
							Brick[brick_counter].setPosition(j * 32, i * 32);
							BrickPositionY[brick_counter] = Brick[brick_counter].getPosition().y;
							BrickPositionX[brick_counter] = Brick[brick_counter].getPosition().x;
							brick_counter++;
						}
						else if (map[i][j] == 3)
						{
							Ladder[ladder_counter].setPosition(j * 32, i * 32);
							ladder_counter++;
						}
						else if (map[i][j] == 6)
						{
							Door.setPosition(j * 32, i * 32);
						}
						else if (map[i][j] == 4)
						{
							Sand[sand_amount].setPosition(j * 32, i * 32);
							sand_amount++;
						}
						else if (map[i][j] == 7)
						{
							DoorMedium.setPosition(j * 32, i * 32);
						}
						else if (map[i][j] == 8)
						{
							DoorHard.setPosition(j * 32, i * 32);
						}
						else if (map[i][j] == 9)
						{
							Steel[steel_counter].setPosition(j * 32, i * 32);
							steel_counter++;
						}
					}
				}
			}

			ConvertSandNum.clear();
			ConvertSandNum.str("");
			ConvertSandNum << sand_amount;
			SandNumber.setString("x " + ConvertSandNum.str());
			openmap.close();
			stage_load = false;
			stage_next = false;
		}

		while (Window.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
			{
				Window.close();
			}
			
			if (Event.type == sf::Event::KeyReleased)
			{
				if (Event.key.code == sf::Keyboard::Z)
				{
					if (!gameover)
					{
						bool destroy = false;
						int brick_index;
						if (Source.y == Left)
						{
							for (int i = 0; i < brick_counter; i++)
							{
								if (Brick[i].getPosition().x == (floor(pHitBox.getPosition().x / 32) - 1) * 32 && Brick[i].getPosition().y == (ceil(pSprite.getPosition().y / 32) + 1) * 32)
								{
									destroy = true;
									if (map[int(Brick[i].getPosition().y) / 32 - 1][int(Brick[i].getPosition().x) / 32] == 2 || map[int(Brick[i].getPosition().y) / 32 - 1][int(Brick[i].getPosition().x) / 32] == 9)
									{
										destroy = false;
									}
									brick_index = i;
								}

								/*if (Brick[i].getPosition().x == (floor(pBoundaryRight.getPosition().x / 32) - 1) * 32 && Brick[i].getPosition().y == (floor(pSprite.getPosition().y / 32)) * 32)
								{
									destroy = false;
									break;
								}*/
							}
						}
						else if (Source.y == Right)
						{
							for (int i = 0; i < brick_counter; i++)
							{
								if (Brick[i].getPosition().x == (ceil(pBoundaryRight.getPosition().x / 32)) * 32 && Brick[i].getPosition().y == (ceil(pSprite.getPosition().y / 32) + 1) * 32)
								{
									destroy = true;
									if (map[int(Brick[i].getPosition().y) / 32 - 1][int(Brick[i].getPosition().x) / 32] == 2 || map[int(Brick[i].getPosition().y) / 32 - 1][int(Brick[i].getPosition().x) / 32] == 9)
									{
										destroy = false;
									}
									brick_index = i;
								}

								/*if (Brick[i].getPosition().x == (floor(pHitBox.getPosition().x / 32) + 1) * 32 && Brick[i].getPosition().y == (floor(pSprite.getPosition().y / 32)) * 32)
								{
									destroy = false;
									break;
								}*/
							}
						}

						if ((Source.y == Right || Source.y == Left) && destroy)
						{
							map[int(Brick[brick_index].getPosition().y)/32][int(Brick[brick_index].getPosition().x)/32] = 0;
							Brick[brick_index].setPosition(-1000, -1000);
							BrickCd[brick_index] = 6;
						}
					}
				}
				else if (Event.key.code == sf::Keyboard::C)
				{
					if (gameover)
					{
					}
					else
					{
						if (pause)
						{
							pause = false;
						}
						else
						{
							pause = true;
						}
					}
				}
				else if (Event.key.code == sf::Keyboard::R)
				{
					if (gameover)
					{
					}
					else
					{
						stage_load = true;
					}
				}
				else if (Event.key.code == sf::Keyboard::Escape)
				{
					Window.close();
				}
			}
		}

		if (pause || gameover)
		{
			if (gameover)
			{
				PauseText.setPosition(600, PauseText.getPosition().y);
				PauseText.setString("Game Over");
			}
			Window.draw(PauseText);
		}
		else
		{
			pBoundaryLeft.setPosition(pSprite.getPosition().x + 1, pSprite.getPosition().y + 14);
			pBoundaryRight.setPosition(pSprite.getPosition().x + 15, pSprite.getPosition().y + 14);
			pBoundaryUp.setPosition(pSprite.getPosition().x + 4, pSprite.getPosition().y + 14);
			pBoundaryDown.setPosition(pSprite.getPosition().x + 4, pSprite.getPosition().y + 16);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				bool moveup = false;
				for (int i = 0; i < ladder_counter; i++)
				{
					if ((pSprite.getPosition().x + 16 >= Ladder[i].getPosition().x && pSprite.getPosition().x + 16 <= Ladder[i].getPosition().x + 32) && (pSprite.getPosition().y <= Ladder[i].getPosition().y + 16 && pSprite.getPosition().y >= Ladder[i].getPosition().y - 32))
					{
						moveup = true;
						break;
					}
				}
				for (int i = 0; i < brick_counter; i++)
				{
					/*if (pBoundaryLeft.getGlobalBounds().intersects(Brick[i].getGlobalBounds()) || pBoundaryRight.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						moveup = false;
						break;
					}*/
					if (pBoundaryUp.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						moveup = false;
						break;
					}
				}
				for (int i = 0; i < steel_counter; i++)
				{
					if (pBoundaryUp.getGlobalBounds().intersects(Steel[i].getGlobalBounds()))
					{
						moveup = false;
						break;
					}
				}

				if (moveup)
				{
					pSprite.move(0, -2);
					Source.y = Up;
					frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
					if (frameCounter >= switchFrame)
					{
						frameCounter = 0;
						Source.x++;
						if (Source.x * 32 >= pTexture.getSize().x)
						{
							Source.x = 0;
						}
					}
				}
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				bool moveleft = true;
				for (int i = 0; i < brick_counter; i++)
				{
					/*if ((pSprite.getPosition().x <= Brick[i].getPosition().x + 33 && pSprite.getPosition().x >= Brick[i].getPosition().x + 32) && (pSprite.getPosition().y >= Brick[i].getPosition().y - 30 && pSprite.getPosition().y <= Brick[i].getPosition().y + 30))
					{
						moveleft = false;
						break;
					}*/
					if (pBoundaryLeft.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						moveleft = false;
						break;
					}
				}
				for (int i = 0; i < steel_counter; i++)
				{
					if (pBoundaryLeft.getGlobalBounds().intersects(Steel[i].getGlobalBounds()))
					{
						moveleft = false;
						break;
					}
				}

				if (moveleft)
				{
					pSprite.move(-3, 0);
					Source.y = Left;
					frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
					if (frameCounter >= switchFrame)
					{
						frameCounter = 0;
						Source.x++;
						if (Source.x * 32 >= pTexture.getSize().x)
						{
							Source.x = 0;
						}
					}
				}
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				bool movedown = false;
				for (int i = 0; i < ladder_counter; i++)
				{
					if ((pSprite.getPosition().x + 16 >= Ladder[i].getPosition().x && pSprite.getPosition().x + 16 <= Ladder[i].getPosition().x + 32) && (pSprite.getPosition().y <= Ladder[i].getPosition().y + 16 && pSprite.getPosition().y >= Ladder[i].getPosition().y - 34))
					{
						movedown = true;
						break;
					}
				}
				for (int i = 0; i < brick_counter; i++)
				{
					/*if (pBoundaryLeft.getGlobalBounds().intersects(Brick[i].getGlobalBounds()) || pBoundaryRight.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						movedown = false;
						break;
					}*/
					if (pBoundaryDown.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						movedown = false;
						break;
					}
				}
				for (int i = 0; i < steel_counter; i++)
				{
					if (pBoundaryDown.getGlobalBounds().intersects(Steel[i].getGlobalBounds()))
					{
						movedown = false;
						break;
					}
				}

				if (movedown)
				{
					pSprite.move(0, 2);
					Source.y = Down;
					frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
					if (frameCounter >= switchFrame)
					{
						frameCounter = 0;
						Source.x++;
						if (Source.x * 32 >= pTexture.getSize().x)
						{
							Source.x = 0;
						}
					}
				}
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				bool moveright = true;
				for (int i = 0; i < brick_counter; i++)
				{
					/*if ((pSprite.getPosition().x >= Brick[i].getPosition().x - 31 && pSprite.getPosition().x <= Brick[i].getPosition().x - 29) && (pSprite.getPosition().y >= Brick[i].getPosition().y - 30 && pSprite.getPosition().y <= Brick[i].getPosition().y + 30))
					{
						moveright = false;
						break;
					}*/
					if (pBoundaryRight.getGlobalBounds().intersects(Brick[i].getGlobalBounds()))
					{
						moveright = false;
						break;
					}
				}
				for (int i = 0; i < steel_counter; i++)
				{
					if (pBoundaryRight.getGlobalBounds().intersects(Steel[i].getGlobalBounds()))
					{
						moveright = false;
						break;
					}
				}

				if (moveright)
				{
					pSprite.move(3, 0);
					Source.y = Right;
					frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
					if (frameCounter >= switchFrame)
					{
						frameCounter = 0;
						Source.x++;
						if (Source.x * 32 >= pTexture.getSize().x)
						{
							Source.x = 0;
						}
					}
				}
			}

			pHitBox.setPosition(pSprite.getPosition().x + 8, pSprite.getPosition().y + 16);
			gravity_pull(pSprite, pHitBox, Brick, Steel, Ladder, brick_counter, steel_counter, ladder_counter, AccelClock, acceleration);
			pHitBox.setPosition(pSprite.getPosition().x + 8, pSprite.getPosition().y + 16);

			if (RespawnClock.getElapsedTime().asSeconds() >= 1)
			{
				RespawnClock.restart();
				for (int i = 0; i < brick_counter; i++)
				{
					if (BrickCd[i] > 1)
					{
						BrickCd[i] = BrickCd[i] - 1;
					}
					else if (BrickCd[i] == 1)
					{
						Brick[i].setPosition(BrickPositionX[i], BrickPositionY[i]);
						map[int(Brick[i].getPosition().y) / 32][int(Brick[i].getPosition().x) / 32] = 2;
						BrickCd[i] = BrickCd[i] - 1;
					}
				}

				if (stage_number != 0 && stage_number != 99)
				{
					convert_minute.str("");
					convert_second.str("");
					convert_minute.clear();
					convert_second.clear();

					if (timer > 0)
					{
						timer--;
					}

					timer_minute = timer / 60;
					timer_second = timer - timer_minute * 60;

					convert_minute << timer_minute;
					convert_second << timer_second;

					if (timer_second < 10)
					{
						timer_sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
					}
					else
					{
						timer_sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
					}
					timer_text.setString(timer_sentence);
				}
			}

			if (sand_amount == sand_collected)
			{
				Door.setTexture(&DoorOpen);
				DoorMedium.setTexture(&DoorOpen);
				DoorHard.setTexture(&DoorOpen);
				if (pHitBox.getGlobalBounds().intersects(Door.getGlobalBounds()))
				{
					completed_staged[completed_stage] = stage_number;
					stage_load = true;
					stage_next = true;
					completed_stage++;
				}
				else if (pHitBox.getGlobalBounds().intersects(DoorMedium.getGlobalBounds()))
				{
					completed_staged[completed_stage] = stage_number;
					stage_load = true;
					stage_next = true;
					completed_stage++;
					difficulty = 2;
				}
				else if (pHitBox.getGlobalBounds().intersects(DoorHard.getGlobalBounds()))
				{
					completed_staged[completed_stage] = stage_number;
					stage_load = true;
					stage_next = true;
					completed_stage++;
					difficulty = 3;
				}
			}

			if (timer == 0)
			{
				gameover = true;
			}
		}
		
		Window.draw(timer_text);
		Window.draw(SandNumber);
		Window.draw(StageText);
		Window.draw(SandDisplay);
		if (completed_stage == 0)
		{
			Window.draw(DoorMedium);
			Window.draw(DoorHard);
		}
		Window.draw(Door);

		for (int i = 0; i < sand_amount; i++)
		{
			if (pHitBox.getGlobalBounds().intersects(Sand[i].getGlobalBounds()))
			{
				Sand[i].setPosition(-1000, -1000);
				sand_collected++;

				ConvertSandNum.clear();
				ConvertSandNum.str("");
				ConvertSandNum << sand_amount - sand_collected;
				SandNumber.setString("x " + ConvertSandNum.str());
			}
			Window.draw(Sand[i]);
		}

		for (int i = 0; i < ladder_counter; i++)
		{
			Window.draw(Ladder[i]);
		}

		for (int i = 0; i < brick_counter; i++)
		{
			Window.draw(Brick[i]);
		}

		for (int i = 0; i < steel_counter; i++)
		{
			Window.draw(Steel[i]);
		}

		pSprite.setTextureRect(sf::IntRect(Source.x * 32, Source.y * 32, 32, 32));
		Window.draw(pSprite);
		Window.display();
		Window.clear();
	}
}